package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
)

func main() {
	flag.Parse()

	count := map[rune]int{}

	args := flag.Args()

	for _, arg := range args {
		var r io.ReadCloser
		var err error

		if arg == "-" {
			r = os.Stdin
		} else {
			r, err = os.Open(arg)
			if err != nil {
				log.Fatal(err)
			}
			defer r.Close()
		}

		Count(r, count)
	}

	runes := []rune{}
	for k, c := range count {
		if c > 0 {
			runes = append(runes, k)
		}
	}
	sort.Slice(runes, func(i, j int) bool { return runes[i] < runes[j] })
	for _, k := range runes {
		fmt.Printf("%U %d\n", k, count[k])
	}
}

func Count(reader io.Reader, count map[rune]int) {
	in := bufio.NewReader(reader)

	for {
		r, _, err := in.ReadRune()
		if err != nil {
			break
		}
		count[r] += 1
	}
}
